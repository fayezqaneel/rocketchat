(function() {
    'use strict';
    import { Meteor } from 'meteor/meteor';
  
    // Login with knowldege center token
    Meteor.loginWithUnymira = function(token, callback) {
      Accounts.callLoginMethod({
        methodArguments: [{
          token: token
        }],
        userCallback: callback
      });
    };
  
  })();