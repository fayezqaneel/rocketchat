// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by unymira-custom-themes.js.
import { name as packageName } from "meteor/unymira-custom-themes";

// Write your tests here!
// Here is an example.
Tinytest.add('unymira-custom-themes - example', function (test) {
  test.equal(packageName, "unymira-custom-themes");
});
