Package.describe({
  name: 'unymira-custom-themes',
  version: '0.0.6',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});


Package.onUse(function(api) {
  api.versionsFrom('1.8.0.2');
  api.use('ecmascript');
  api.use([
    'ecmascript',
		'webapp',
		'templating',
    'less',
  ]);

  api.use('accounts-password');
  api.addFiles('unymira_login_client.js', ['client']);
  api.addFiles('unymira_login_server.js', ['server']);
  api.export('UnymiraLogin', ['server', 'client']);

  api.addFiles('assets/layout.html', 'client');
  api.addFiles('assets/theme.css', 'client');
  api.addFiles('assets/theme.js', 'client');
  api.addFiles("assets/logo.png","client",{
    isAsset: true
  });
  api.mainModule('unymira-custom-themes.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('unymira-custom-themes');
  api.mainModule('unymira-custom-themes-tests.js');
});
