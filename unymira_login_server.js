/* global UnymiraLogin: true */
import { Meteor } from 'meteor/meteor';
var crypto = require('crypto');

var UnymiraLogin = {};

var keyBase64 = "ZEF0QWJBc0U5ODc2NTQzNA==";
var ivBase64 = 'AcynMwikMkW4c7+mHtwtfw==';

// token expiration time in mintues
var tokenExpireTime = 60;

// decrypt data funtion 
UnymiraLogin.getDecryptData = function (text){
  var dec = UnymiraLogin.decrypt(decodeURIComponent(text), keyBase64, ivBase64);
  return dec.split(',');
}


UnymiraLogin.getAlgorithm = function(keyBase64) {

    var key = Buffer.from(keyBase64, 'base64');
    switch (key.length) {
        case 16:
            return 'aes-128-cbc';
        case 32:
            return 'aes-256-cbc';

    }

    throw new Error('Invalid key length: ' + key.length);
}

UnymiraLogin.decrypt = function(messagebase64, keyBase64, ivBase64) {
    messagebase64 = messagebase64.replace(/ /g,"+");
    console.log(messagebase64, 'messagebase64');
    var key = Buffer.from(keyBase64, 'base64');
    var iv = Buffer.from(ivBase64, 'base64');

    var decipher = crypto.createDecipheriv(UnymiraLogin.getAlgorithm(keyBase64), key, iv);
    let dec = decipher.update(messagebase64, 'base64');
    dec += decipher.final();
    return dec;
}

// generate random password from 12 characters
UnymiraLogin.getRandomPassword = function (){
    return Math.random().toString(36).slice(-12);
}

// calculate token expiration time
UnymiraLogin.getTimeDiff = function(startDate, endDate){
    var diff = endDate.getTime() - startDate.getTime();
    return (diff / 60000);
};

// Register a login handler
Accounts.registerLoginHandler("unymira_login", function(options) {

  // Payload should be of form email, username, name, timestamps
  
  var userdata = UnymiraLogin.getDecryptData(options.token);
  var email = userdata[0];
  var username = userdata[1];
  if (username.startsWith('/')){
    username = username.replace('/', '');
  }
  var name = userdata[2];
  var timestamps = userdata[3];

  timestamps = new Date(timestamps * 1000);

  var now = new Date();

  // calculate the time diffrence
  var diff = parseInt(UnymiraLogin.getTimeDiff(timestamps, now));

  // Find the actual user objec/
  var user = Meteor.users.findOne({
    'emails.address': email
  });

  // Existing user => verify user if necessary
  if (user) {
    
    if (diff > tokenExpireTime) {
      // if time diffrence is more than the expiration time then logout
      Meteor.call('logoutCleanUp', user);
      Meteor.users.update({ _id: user._id }, { $set: { "services.resume.loginTokens": [] } });
      return false;
    }
    // Return login result for this user
    return {
      userId: user._id
    };
  } else {
      // if there is no user with that email create new one and update his roles
      var userid = Accounts.createUser({
          username: username,
          email: email,
          password: UnymiraLogin.getRandomPassword(),
          profile: {
            name: name,
          }
      });
      Meteor.users.update(userid, {$set:{roles:['livechat-agent', 'user']}});
      return {
          userId: userid
      };
  }
  
  // Else no user. Throw error so client knows to init user creation
  return {
    error: new Meteor.Error(404, 'user-not-found')
  };
});

 

