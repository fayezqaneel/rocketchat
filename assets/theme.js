import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import { Template } from 'meteor/templating';

if (Meteor.isClient && window.location.href.indexOf('/livechat') !== -1 ) {
    Template.messages.onRendered(function() {
        console.log('testtttttt');
    });
}



import { FlowRouter } from 'meteor/kadira:flow-router';

FlowRouter.route('/livechat-unymira', {
	name: 'index',
	action() {
        BlazeLayout.render('livechatUnymira', { center: 'livechatWindow' });
	},
});


Meteor.startup(function() {
    var token = getParameterByName('token');
    if(token){
        Meteor.loginWithUnymira(token, function(){
            console.log('login error');
        });
    }
});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
window.onload = function () {
    $('body').addClass(getParameterByName('theme'));
}